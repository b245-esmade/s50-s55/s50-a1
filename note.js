//S50 React.js - Component Driven Development

// What is React.Js?
	// Javascript library for building user interfaces that is
		// Declarative: makes you write code with question of WHAT instead of HOW. Allows code to be more predictable and easier to debug.
		// Component-Based: Allows to write reusable, complex UI components in a quick and efficient manner
		// Learn Once, Write Anywhere: React can be used on the server using Node and power mobile apps using React Native



// initial step
	/*commands
		npx create-react-app react-app //project-name
		cd react-app
		npm start

		//install bootstrap
		npm i react-bootstrap bootstrap */
	


// files to remove in src
	/*App.test.js
	index.css
	reportWebVitals
	logo.svg
	*/

// update index.js and app.js and remove the src files that was removed



// sublime to-do
	// install package control
	// babel > Javascript > Javascript Babel


// the "import" statement allows us to use the code/export modules from other files similar to how we use the "require" function in NodeJS



1. create AppNavBar or file for your component

	export default function Banner(){
	return(
		)
}


2. visit https://react-bootstrap.github*
		-install npm install react-bootstrap bootstrap*
		-import css bootstrap in index.js
4. import Bootstrap classes in you component file (ex. AppNavBar.js)
		-make sure "export default function AppNavBar()" is set so file can be exported, see #1
5. copy-paste the component link from bootstrap to component file
6. in App.js import the component file(ex. AppNavBar.js)
		
		import AppNavBar from './AppNavBar.js'

			function App() {
			  return (
			    <AppNavBar/>
			  );
			} 



// React JS it applies concepts of rendering and mounting in order to display and create components

			rendering> process of calling/invoking component
			mounting> renders/displays component in webpage


<AppNavBar/> //Accross all pages


create Home for the ff components

			<Banner/>
			<Highlights/>





// S51 React.js - Props and States
	// Props = Properties
		// allows components to be reusable


// S52 States = Data in components
	// Hooks
			// functions that allow developers to create and manage lifecycle within components

	// Props
			// passing of data from one component to another

		// [UseState]
			// to access state/variable internally in a component


				import {useState} from 'react';

			/* Syntax
				  /variable,function         /initial value
			const [getter,setter] = useState(initialGetterValue);

			*/
					// state
			const [enrollees, setEnrollees] = useState(0);
			const [seats, setSeats] = useState(30);





		// [UseEffect]
			// allows execute a piece of code whenever a component gets rendered to the page or if value state changes
			// side effects =  function

				/*Syntax
					useEffect(() => {
					},[dependencies])
				*/


		// using the event.target.value will capture the value inputted by user on our input area

			// Dependencies in useEffect
				// 1. Single dependency [dependency]
					// on the initial load of the component, the side effect will be triggered whenever changes occured
				// 2. Empty dependency []
					// the sideeffect will only run during the initial load
				// 3. Multiple dependecies [dependency1,dependency2]
					// the side effect will run during the initial and whenever the state of the dependencies change




// Routing and Conditional Rendering
	// react-router-dom


// install react router dom
	npm install react-router-dom

	// for use to be able to use the modelue/library accross all of our pages we have to contain "BrowserRouter" on a variable "Router"

	// Routes - we contain all the routes in our react-app
	// Route - specific route wherein we will declare

	// Link - if we want to go from one page to another page
	// NavLink - if we want to change our page using our NavBar


// LocalStorage
	// 'localStorage.setItem' allows us to manipulate the browser's localStorage property to store infromation indefinitely



// React.js AppState Management

	// UseContext - make data available across all components
	




// Sweet Alert/Pop Up Message
	// Install
		npm i sweetalert2
		https://sweetalert2.github.io/


// Environment variable
	// 