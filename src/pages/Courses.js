		// variable inside this address file
// import coursesData from '../database/courses.js';
import CourseCard from '../components/CourseCard.js';
import {Fragment, useEffect,useState} from 'react';


export default function Courses(){
	// console.log(coursesData)


// create variable named "courses" to iterate course cards
	/*const courses = coursesData.map( course => {

		return ( <CourseCard key = {course.id} courseProp = {course}/> )


	})*/

// useState
const [courses, setCourses] = useState([]);


// useEffect
	useEffect(()=>{
		// fetch all courses from API-DB
		fetch(`${process.env.REACT_APP_API_URL}/course/allActive`)
		.then(result => result.json())
		.then(data => {
			console.log(data)

			//to change value of courses
			setCourses(data.map(course =>{
				return ( <CourseCard key = {course._id} courseProp = {course}/> )
			}))
		})

	},[])



	return(
		<Fragment>
			<h1 className="text-center mt-3"> Courses </h1>
			{courses}
		</Fragment>
		
		)
}