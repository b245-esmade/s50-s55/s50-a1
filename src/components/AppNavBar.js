// import Boostrap classes
import Container from 'react-bootstrap/Container';
import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';

/*import {Container, Nav, NavBar} from 'react-bootstrap'*/

import {Link, NavLink} from 'react-router-dom'
import {useContext, Fragment} from 'react'
import UserContext from '../UserContext.js'

// to export
export default function AppNavBar(){
	// to get items/content in localStorage
	// .getItem('property')
	console.log(localStorage.getItem('email'))

	// useState for user
	// const [user,setUser] = useState(localStorage.getItem('email'))

// UseContext
	const {user} = useContext(UserContext)

	return(
		// insert Bootstrap link here
		<Navbar bg="light" expand="lg">
		      <Container>
		      {/*using 'as' keyword, Navbar inherits link function*/}
		      {/*the 'to' keyword links the Navbar.Brand to the declared url endpoint*/}
		        <Navbar.Brand as ={Link} to ='/'>Zuitt</Navbar.Brand>
		        <Navbar.Toggle aria-controls="basic-navbar-nav" />
		        <Navbar.Collapse id="basic-navbar-nav">
		          <Nav className="ms-auto">
		            <Nav.Link as = {NavLink} to ='/'>Home</Nav.Link>
		            <Nav.Link as = {NavLink} to ='/Courses'>Courses</Nav.Link>

		            {/*Conditional Rendering*/}

		            {
		            	user ?
		            	 <Nav.Link as = {NavLink} to='/logout'>Logout</Nav.Link>
		            	 :
		            	<Fragment>
		            	  <Nav.Link as = {NavLink} to ='/Register'>Register</Nav.Link>

		            	 <Nav.Link as = {NavLink} to ='/Login'>Login</Nav.Link>


		            	</Fragment>
		            }

		           
		            
		          </Nav>
		        </Navbar.Collapse>
		      </Container>
		    </Navbar>
		)
}