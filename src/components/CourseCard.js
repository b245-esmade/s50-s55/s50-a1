// import Button from 'react-bootstrap/Button';
// import Card from 'react-bootstrap/Card';
import {Button, Card, Row, Col} from 'react-bootstrap'
import {useState, useEffect, useContext} from 'react';
import UserContext from '../UserContext.js'
import {Link} from 'react-router-dom'

export default function CourseCard({courseProp}){
// OR export default function CourseCard({course}){
	console.log(courseProp);


	// destructure
	const {_id,name,description,price} = courseProp;


// Use the state hook  for this component to be able to store its state

	/* Syntax
		  /variable,function         /initial value
	const [getter,setter] = useState(initialGetterValue);

	*/
			// list of states for enrollees and seats
	const [enrollees, setEnrollees] = useState(0);
	const [seats, setSeats] = useState(30);

	// add new state
	const [isDisabled,setIsDisabled] = useState(false)

	// initial value of enrollees state
		// console.log(enrollees)

	// to change/reassign the value of state
		// setEnrollees(1)
		// console.log(setEnrollees)


const {user} = useContext(UserContext)

	function enroll(){
		if (seats > 1 && enrollees <29){
			setEnrollees(enrollees+1)
			setSeats(seats-1)	
		} else{
			alert ("Congratulations for making it to the cut of enrollees!")
			// just forsake of disabling button and prompting alert message
			setEnrollees(enrollees+1)
			setSeats(seats-1)
		}
	}

	// Define a 'useEffect' hook to have the 'CourseCard' component do perform a certain task

	/*Syntax

			useEffect(sideEffect/function, [dependencies]);

	*/

		// sideEffect/function - will run on the first load and will reload depending on the dependecy array



	useEffect(()=> {
		if(seats === 0){
			setIsDisabled(true);
		}
	},[seats])



	return (

		<Row className = "mt-0 p-5">
				{/*Course Cards*/}
			<Col>
				<Card>
				     <Card.Body>
				       <Card.Title>{name}</Card.Title>
				       <Card.Subtitle>Description</Card.Subtitle>
				       	<Card.Text>{description}</Card.Text>
				       <Card.Subtitle>Price</Card.Subtitle>
				       	<Card.Text>{price}</Card.Text>

				       <Card.Subtitle>Enrollees</Card.Subtitle>
				       	<Card.Text>{enrollees}</Card.Text>

				       	<Card.Subtitle>Available Seats:</Card.Subtitle>
				       	<Card.Text>{seats}</Card.Text>

{/*Ternary condition*/}

				       	{//if user is logged in
				       		user?

				        <Button as = {Link} to = {`/course/${_id}`} disabled={isDisabled} variant="primary">See details</Button>
				        :
				        //if user is logged out
				        <Button as = {Link} to ='/Login' variant="primary">Login</Button>


				        }


				     </Card.Body>
				   </Card>
				</Col>
		</Row>
		)
}