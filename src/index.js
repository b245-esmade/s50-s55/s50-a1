import React from 'react';
import ReactDOM from 'react-dom/client';
// import './index.css';

// import bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';
// import App in App.js
import App from './App';
// import reportWebVitals from './reportWebVitals';

// we created an element with an ID 'root'
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals


// reportWebVitals();


/*const name = "John Smith";
const element = <h1> Hello, {name}</h1>

root.render(
  element)*/